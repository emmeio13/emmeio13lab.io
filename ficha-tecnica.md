---
title: Ficha Técnica
layout: default
---

<div class="container-fluid">
  <div class="row">
<div class="ficha-tecnica col-sm-10">
  <h1>Ficha Técnica</h1>
  <p><b>Organização e curadoria:</b> Artur Cabral e Joenio Costa</p>
  
  <p><b>Assistentes de produção técnica e executiva:</b> Equipe MediaLab/UNB</p>
  <p><b>Organização White Page Gallery:</b> Lynn Carone</p>

  <p><b><a href="mailto:medialabunbdf@gmail.com">Fale com a gente ;)</a></b></p>
  <p>
    <b>Artistas:</b><br>
    <span>
      1mulherporM2 {Adriana Bertini, Ana Roberta Lima,<br>Bia Parreiras, Cami Onuki, Carla Venusa,<br>Fernanda Klee, Fulvia Molina, Iara Venanzi,<br>Karen Caetano, Laura Corrêa, Lucrécia Couso,<br>Lynn Carone, Marcia Gadioli, Melissa Haidar,<br>Paula Marina, Tina Leme Scott} <br>
      Alexandre Rangel <br>
      Alexandre Villares <br>
      Amanda Laubmeier <br>
      Ana Hoeper <br>
      Antenor Ferreira <br>
      Artur Cabral <br>
      Autonomia Duvidosa {Ana Carolina Pinheiro, Aníbal Diniz, Débora Passos,<br>Filipe Leroy, Jackson Marinho, Joanna Ramos,<br>Victor Valentim}<br>
      Belister Paulino <br>
      BSBLOrk <br>
      Camila Leite <br>
      Ceci Vilca <br>
      Clara Acioli <br>
      COM6 {Agda Carvalho, Clayton Policarpo, Daniel Malva,<br>Miguel Alonso e Sergio Venancio} <br>
      Edgar Franco <br>
      Fabricio Duarte <br>
      Fanqi Zeng <br>
      Fernando Fuão <br>
      Fernando Pericin <br>
      Grupo Realidades {Bruna Mayer, Clayton Policarpo, Dario Vargas,<br>Felipe Mamone, Lívia Gabbai, Letícia Brasil,<br>Loren Bergantini, Luca Ribeiro, Miguel Alonso,<br>Paula Perissinotto, Sergio Venancio,<br>Silvia Laurentiz} <br>
      Guilherme Lazzaretti <br>
      Izadora Alves <br>
      Jackson Marinho <br>
      José Loures <br>
      LabFront {Pablo Gobira e Antônio Mozelli}<br>
      Leandro Ramalho (Caju) <br>
      Léo Pimentel <br>
      Lilian Amaral<br>
      Luis Mata <br>
      Lynn Carone <br>
      Malu Fragoso <br>
      Marília Bergamo <br>
      MIRELLA X MUEP <br>
      Monica Rizolli <br>
      Nadine Nicolay <br>
      Naveen Srivatsav <br>
      Paulo Valeriano <br>
      Prahlada Hargreaves <br>
      Rafael Turatti <br>
      Raísa Curty <br>
      Rejane Cantoni <br>
      Sandra Rey <br>
      Sara Matos <br>
      Sergio Venancio <br>
      Suzete Venturelli <br>
      Teleportation {Martín Groisman, Fabricio Costa, Dante Saez,<br>Aimé Guzmán, Alejandro Kauderer,<br>Javier Pistani, Mica Schiaffino, Lucila Sol,<br>Bia Borges e Gustavo Luppi}<br>
      Val Sampaio <br>
    </span>
  </p>
</div>
</div>
</div>


