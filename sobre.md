---
title: Sobre
layout: default
---

<div class="container-fluid">
  <div class="row">
    <div class="sobre col-sm-10">
      <h5>
      No contexto global da pandemia Covid-19, onde é questionado nossas organizações sociais, de saúde, econômicas, nacionais e transnacionais e que perturba nossas referências culturais, que papéis, possivelmente novos, a arte poderia desempenhar? A web-exposição EmMeio#13: Contaminações reúne uma miscelânea de trabalhos poéticos de artistas, coletivos e grupos acadêmicos, nacionais e internacionais, que respondem aos complexos problemas que surgiram durante a pandemia. A exposição propõe um tipo de contaminação pela cultura, à qual estamos suscetíveis de uma maneira ou de outra. As obras aqui apresentadas, explicitam a potência dos processos de fabulação da arte em explorar novas relações entre o humano e o mundo, em um planeta progressivamente contaminado, desde os mares, rios, solos e almas. <br>
  		Nesse cenário, estas obras refletem a capacidade da prática poética em ficcionar o real e especular as virtualidades, por meio de imagens, vídeos, performances e aplicações interativas. Trabalhos que nos ajudam a sobreviver a esses tempos conturbados, no qual somos ameaçados constantemente com o risco de novas contaminações, sejam elas por vírus, por pensamentos ou por práticas nocivas à humanidade. Este evento online é uma oportunidade para a comunidade em geral interagir, ver, ouvir e se envolver com os artistas, pois eles apresentam uma ampla gama de reflexões e propostas, para discutir sobre o lugar da criatividade em relação ao momento em que vivemos em um contexto histórico, inédito, e contemporâneo. 
      </h5>
      <p class='nome-autor'>Artur Cabral , 2021</p>
    </div>
  </div>
</div>
