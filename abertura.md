---
title: abertura
layout: default
---
<script>
var target_date = new Date("jun 15, 2021").getTime(); 
var dias, horas, minutos, segundos;
var regressiva = document.getElementById("regressiva");

setInterval(function () {

    var current_date = new Date().getTime();
    var segundos_f = (target_date - current_date) / 1000;

dias = parseInt(segundos_f / 86400);
    segundos_f = segundos_f % 86400;
    
    horas = parseInt(segundos_f / 3600);
    segundos_f = segundos_f % 3600;
    
    minutos = parseInt(segundos_f / 60);
    segundos = parseInt(segundos_f % 60);

    document.getElementById('dia').innerHTML = dias;
document.getElementById('hora').innerHTML = horas;
document.getElementById('minuto').innerHTML = minutos;
document.getElementById('segundo').innerHTML = segundos;
  

}, 1000);
</script>
<style>
  .contagem{
width:300px;
height:70px;
margin: 8em auto 0;
}

.numero
{
min-width: 20px;
max-width: 55px;
background-color: #efefef;
color: #000000;
font-size: 22px;
margin: 5px;
text-align: center;
border-radius: 5px;
padding: 5px;
}

.legenda{
height: 25px;
line-height: 10px;
font-size:12px;

text-align: center;
}

</style>

<div class="container-fluid">
  <div class="row">
    <div class="ficha-tecnica col-sm-10">
    <div class="text-center">
    <img src="/assets/abertura.gif" class="img-fluid" alt="ABERTURA / OPENING "/> 
    </div>
  


  <div class="contagem">
  <table><tr><td><div class="numero" id="dia"></div></td><td><div class="numero" id="hora"></div></td><td><div class="numero" id="minuto"></div></td><td><div class="numero" id="segundo"></div></td></tr>
  <tr style="height:20px"><td><p class="legenda">dias</p></td><td><p class="legenda">horas</p></td><td><p class="legenda">min</p></td><td><p class="legenda">seg</p></td></tr></table>
  </div>

  </div>
  </div>
</div>


