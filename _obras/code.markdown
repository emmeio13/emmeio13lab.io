---
layout: obra
title: CODE
thumbnail: /assets/thumbnail/t-code.png
artista: Rejane Cantoni / Mirella X Muep
duo-bio: Rejane Cantoni <a href="https://www.cantoni-crescenti.com.br/">cantoni-crescenti.com.br </a><br> Mirella X Muep <a href="http://www.mirellabrandixmuepetmo.com/">mirellabrandixmuepetmo.com</a> 
texto-descricao: CODE é um aplicativo desenhado para traduzir sons em letras do alfabeto romano.<br> CODE obedece às seguintes regras e configurações&#58;<br><br> frequência <br> 1.todo som com frequência menor ou igual a minPitch é igual a 'A'&#59; <br>2.todo som com frequência maior ou igual a maxPitch é igual a 'Z'&#59; <br>3.qualquer frequência entre minPitch e maxPitch se transforma em uma letra entre ‘A’ e ‘Z’, em função de sua correspondência linear.<br> <br>intensidade<br> 1.todo som com intensidade menor ou igual a minRMS, tem o menor tamanho de fonte&#59; <br>2.todo som com intensidade maior ou igual a maxRMS, tem o maior tamanho de fonte&#59; <br>3.qualquer intensidade entre minRMS e maxRMS se transforma em uma letra entre ‘A’ e ‘Z’, em função de sua correspondência linear. <br><br>confiança<br> 1.sons com confiança menor que minConfidence são ignorados. algorithm <br>2.o tipo de algoritmo usado na detecção de frequência produz resultados diferentes <br><br> insta&#58; @pinkumbrellas.artresidency
ano: 2021
---
<iframe src="https://player.vimeo.com/video/561586651" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>