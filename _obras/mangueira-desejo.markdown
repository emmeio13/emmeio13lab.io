---
layout: obra
title: Mangueira Desejo - O Duplo
thumbnail: /assets/thumbnail/t-magueira-desejo.png
artista:  Val Sampaio
bio: https://mangueiradesejo.com.br/
texto-descricao: “Mangueira Desejo” é uma instalação/intervenção de arte que cria ambiente de interação digital nas  mangueiras, por intermédio de app para celular de realidade aumentada. Esse aplicativo possibilitará  a inserção de objetos virtuais no ambiente físico que podem ser acessados em tempo real. Essas escritas alimentam uma nuvem virtual de desejos ao redor das mangueiras em realidade aumentada, in sito, em sítio físico e na internet por intermédio da geolocalização das mangueiras mapeadas.
ano: 2021
---
<img src="/assets/externos/magueira-desejo/instalacao.png" loop=infinite alt="Instalação" class="img-fluid mx-auto d-block">

<a href="https://oduplo.mangueiradesejo.com.br/">`Download e Interação` </a>

<iframe src="https://www.youtube.com/embed/19YnXNXqWMY" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
