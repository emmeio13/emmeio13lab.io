---
layout: obra
title:  S__ J_S_ L__R_S
thumbnail: /assets/thumbnail/t-S__ J_S_ L__R_S.png
artista: José Loures
duo-bio: Artista multimídia, professor de Histórias em Quadrinhos, e produtor cultural. Doutor em Artes pela Universidade de Brasília (UnB). Mestre em Arte e Cultura Visual pela Universidade Federal de Goiás (UFG). Trabalha na linguagem da arte computacional, histórias em quadrinhos, webarte, fake arte e gamearte. Também pesquisa sobre transhumanismo, jogos de tabuleiro, cibercultura, sexualidade e práticas divinatórias. Membro do Coletivo Interdisciplinar de Pesquisa em Games (CIPEG) e professor de Artes no Instituto Federal de Educação, Ciência e Tecnologia de São Paulo (IFSP).
texto-descricao: Qual a vida útil de uma mensagem? A imagem é contaminada ou contamina o ambiente? S__ J_S_ L__R_S se debruça sobre essas questões. Uma determinada mensagem foi filmada por mim e refilmada várias vezes através do olhar do Outro. Nesse contexto, o meio escolhido para a realização do trabalho foi o WhatsApp – plataforma que ganhou o protagonismo nos últimos anos, sendo inclusive apontada como um dos principais meios de difusão das fake News no Brasil. O registro das tentativas de sobreviver em um meio sujeito a contaminação e desinformação.
ano: 2021
---
<iframe src="https://www.youtube.com/embed/8gpdh_-vOUM" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>
