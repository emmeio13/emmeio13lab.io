---
layout: obra
title: Biótica Celeste
thumbnail: /assets/thumbnail/t-biotica-celeste.png
artista: Krishna Passos
duo-bio: Krishna Passos é Mestre e Doutor em Arte pelo PPGAV/UnB. Desenvolve pesquisa artística desde o ano 2000 com participações em mostras, salões, residências artísticas e prêmios. Suas pesquisas são híbridas entre arte sonora, videoarte e paisagem sonora em que, enfatiza, a materialidade física dos elementos e suas propriedades, assim, investiga fenômenos vibratórios, possibilidades de gerar sons e imagens com as conjunturas materiais e as suas instabilidades. 
texto-descricao: Biótica Celeste é uma instalação sonora e visual, integrante da série Epicentros, composto por um sistema montado sobre um retroprojetor de transparência, dos anos 80, contendo alto-falante em seu interior, uma pequena ventoinha e placa de Petri (usada em laboratórios). Sobre o retroprojetor é colocada a placa com água. Quando o falante emite o som no interior do retroprojetor (um tom cíclico de 20Hz a 100Hz), ele provoca vibrações e efeitos cimáticos na superfície da água.
ano: 2019
---
<iframe src="https://www.youtube.com/embed/8ixNXd4Lhm0" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
