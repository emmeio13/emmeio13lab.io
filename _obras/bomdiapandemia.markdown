---
layout: obra
title: Bom Dia Pandemia
thumbnail: /assets/thumbnail/t-bomdiapandemia.png
artista:  Prahlada Hargreaves
duo-bio: Prahlada Hargreaves, pralads, 1990, vive e trabalha em Brasília. Constrói coisas que utilizam a luz, o som, o movimento e a materialidade dos componentes eletrônicos que constroem o objeto. Suas pesquisas buscam explorar as relações entre artista, ferramenta, laboratório e a produção eletrônica. Estuda também jardinagem e o uso de sensores elétricos em plantas, utilizados para gerar formas 3D ou controlar funções de estufas. É membro da equipe de pesquisa MediaLab/UnB, e cofundador do coletivo m_labio.
texto-descricao: Bom Dia Pandemia é uma tela onde se vê um programa contando números, intercalados por imagens. Os números contam valores fixados durante a construção do objeto e representam a média móvel, o recorde e o total de mortos durante a pandemia da COVID-19. Estes valores foram captados até o dia 30 de abril de 2021. As imagens são recolhidas de uma lista de xingamentos inventados, que buscam criar adjetivos inéditos para descrever as pessoas que acompanham, apoiam e compõe o governo federal atual (2018-). <br> A tela é construída de uma maneira que atrapalha propositalmente o seu registro. Utiliza lentes de resina que refletem toda a luz, precisando ser vista no escuro.  Sua estrutura e circuito, porém, localizada na parte de trás, é construída por delicados arames suspensos pelas soldas das ligações, e necessita de um ambiente iluminado para ser vista.
ano: 2021
---
<iframe src="https://player.vimeo.com/video/561609604" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>

<i>Código por Prahlada Hargreaves, Artur Cabral e Leandro Caju Ramalho
 </i>