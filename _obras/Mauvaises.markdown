---
layout: obra
title: Mauvaises Herbes, ou... sobre como semear uma floresta na paisagem.
thumbnail: /assets/thumbnail/t-mauvaises.png
artista: Sandra Rey
duo-bio: Artista Plástica. Pesquisadora CNPq. Professora no PPG Artes Visuais da UFRGS. Desenvolve projeto artístico conectando a arte e natureza em produções de imagens fotográficas de grandes e pequenos formatos, vídeos, instalações e livros de artista. Seu trabalho desenvolve-se a partir de caminhadas na natureza e pesquisas em fotografia e tecnologia. O processo artístico implica num vínculo estreito entre a noção de experiência junto à natureza, em articulação com o trabalho em estúdio envolvendo pesquisas formais com processos de montagem, questões técnicas, e reflexão teórica. Expõe, realiza curadorias. Publica textos e artigos sobre questões referentes à pesquisa em Artes Visuais e escritos de artista.
texto-descricao: Animação com recortes de fotografias de inços e montagens de fotografias obtidas em caminhadas na natureza. A animação é um ensaio sobre o conceito de natureza segundo Merleau-Ponty que a define como “aquilo que tem um sentido, sem que esse sentido tenha passado pelo pensamento. É a autoprodução de um sentido”. Como metáfora, o vídeo coloca em movimento imagens que evocam a proliferação e constante transformação dos elementos naturais, e a alternância entre vida e morte, simulando o surgimento e desaparecimento de uma floresta a partir de recortes dos inços, e de imagens de árvores secas sustentadas por um suporte de ferro. A sequência e as passagens entre as imagens e sons buscam o estranhamento a fim de provocar o imaginário do espectador para desalojá- lo de suas percepções.
ano: 2021
---
<iframe src="https://www.youtube.com/embed/dFxhQfYPESw" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

