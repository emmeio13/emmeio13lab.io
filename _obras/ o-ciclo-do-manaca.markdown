---
layout: obra
title: O Ciclo do Manacá
thumbnail: /assets/thumbnail/t-o-ciclo-do-manaca.png
artista: Monica Rizzolli
duo-bio: Monica Rizzolli é artista-programadora, estudou no Instituto de Artes da UNESP e na Kunsthochschule Kassel (Alemanha). Participou de projetos internacionais como MAK Center Artists and Architects (EUA), Creatives in Residence (China), Sweet Home (Espanha) e A.I.R. DRAWinternational (França). Recebeu o prêmio MAK Schindler, do MAK – Museum of Applied Arts Vienna. É co-fundadora da Noite de Processing e co-organizadora do Processing Community Day BR, evento anual da Processing Foundation. É sócia da type foundry Just in Type e do estúdio de design Contrast – programação, tipografia e design. http://contrast.parts
texto-descricao: O Tríptico "O Ciclo do Manacá" é composto por 03 GIFS e faz parte da série Jardim Tropical que explora a relação entre a flora brasileira e a geometria. O padrão de fundo foi feito com um algoritmo WaveFunctionCollapse Overllaping e a transformação da flor foi gerada usando um arquivo .svg (desenhado no Illustrator) modificado usando a biblioteca Geomerative no Processing.
ano: 2021
---
<img src="/assets/externos/o-ciclo-do-manaca/origin.gif" loop=infinite alt="Tibouchina mutabilis, origin" class="img-fluid mx-auto d-block">
<br>

`Tibouchina mutabilis, origin` 
<br><br>
<img src="/assets/externos/o-ciclo-do-manaca/life.gif" loop=infinite alt="Tibouchina mutabilis, life" class="img-fluid mx-auto d-block">
<br>

`Tibouchina mutabilis, life` 
<br><br>
<img src="/assets/externos/o-ciclo-do-manaca/death.gif" loop=infinite alt="Tibouchina mutabilis, death" class="img-fluid mx-auto d-block">
<br>

`Tibouchina mutabilis, death` 
<br>