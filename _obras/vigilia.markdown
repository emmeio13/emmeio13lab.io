---
layout: obra
title: VIGÍLIA
thumbnail: /assets/thumbnail/t-vigilia.png
artista: Lilian Amaral e Fernando Fuão
duo-bio: Projeto de Vigília Audiovisual de Lilian Amaral (DIVERSITAS USP) em colaboração com Fernando  Fuão (UFRGS), 8 de Março, 2021. 14'41''. <br> Pesquisa e narração&#58; LILIAN AMARAL <br> Musica&#58; Sakafu. TEX (Tura) <br>Video&#58; Fernando Fuão
texto-descricao: Essa quase-oração é dedicada às mulheres Trans, vítimas do feminicídio, vítimas da violência (Lilian Amaral, 2021). <br> I.<br> Vigília – Substantivo feminino. Etimologia&#58; a palavra vigília deriva do latim vigilia,ae, que significa guarda ou vigia, privação de sono. <br>1. Vigília é a ação de estar desperto. Trata-se, neste sentido, de um estado de consciência que antecede o sono&#59; <br>2. Estado de quem permanece acordado durante a noite&#59; vela&#59; <br>3. Meditação ou trabalho intelectual nas horas destinadas ao repouso&#59; lucubração&#59; <br>4. Celebração noturna na véspera de festa importante&#59; <br>5. Concentração em lugar público por um longo período (que geralmente se estende de modo a abarcar uma ou mais noites), efetuada como forma de protesto, homenagem, etc.&#59; <br>6. Significado religioso&#58; os romanos dividiam a noite em quatro vigílias, com 3 horas cada uma, correspondendo a cada um dos quatro períodos em que se divide a noite&#59; <br>7. Desvelo&#59; cuidado. <br>II.<br> VIGÍLIA – Vídeo-Vigília elaborado para integrar a VIGÌLIA ON LINE proposta por mulheres artistas iberoamericanas, como forma de  denúncia e re-existência contra todas as formas de violência a que as mulheres são submetidas de modo contínuo, acentuado, crescente e naturalizado.  Performance duracional coletiva em rede, proposta de prática poética-política, reflexão crítica em torno do &quot;dia internacional da Mulher&quot;, realizada durante 24 horas entre 08 e 09 de maio de 2021.
ano: 2021
---
<iframe src="https://player.vimeo.com/video/561571931" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>

<i>8 de Março, 2021. 14'41''.</i><br> 
<b>Pesquisa e narração:</b> LILIAN AMARAL<br> 
<b>Musica:</b> Sakafu. TEX (Tura)<br> 
<b>Video:</b> Fernando Fuão<br> 
