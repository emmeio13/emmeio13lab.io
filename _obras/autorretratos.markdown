---
layout: obra
title: Autorretratos
thumbnail: /assets/thumbnail/t-autoretratos.png
artista: Sergio Venancio
bio:  https://sergiovenancio.art/
texto-descricao: O trabalho é uma provocação sobre a expressão 'autorretrato' e o fazer artístico contemporâneo, inegavelmente contaminado pelo digital. Vivemos em tempos de automação de tarefas cognitivas em escalas individual e coletiva. Proposições poéticas podem ser geradas pela expressão, presença e extensão algorítmicas do artista. O 'auto' é dúbio&#58; o autorreferente se confunde com o autoprojetado e o automatizado, manifestados nas máquinas e nas múltiplas vivências digitais. São outros modos e momentos de ser e estar, outras ativações. Faça e seja feito seu autorretrato.<br>(Nenhuma informação, fotografia ou biometria digital é armazenada e/ou compartilhada nesta aplicação. Você pode guardar os desenhos, mas caso queira compartilhar em redes sociais, por favor, marque o perfil @sergiovenancio.art no Instagram ou @servenancio no Twitter)
ano: 2021 
---

<iframe class="frame" allow="camera;microphone" src="https://sergiovenancio.art/autorretratos/"></iframe>
<br>
<a href="https://sergiovenancio.art/autorretratos/" target="_blank">TELA INTEIRA</a>