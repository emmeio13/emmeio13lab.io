---
layout: obra
title: Lave as mãos
thumbnail: /assets/thumbnail/t-lave-as-maos.png
artista: Pablo Gobira e Antônio Mozelli
duo-bio: <b>Pablo Gobira</b> é professor da Escola Guignard (UEMG), do PPGArtes (UEMG) e do PPGGOC (UFMG). Pesquisador Produtividade (CNPq). Membro pesquisador e gestor de serviços da Rede Brasileira de Serviços de Preservação Digital (IBICT/MCTIC). Coordenador do grupo de pesquisa (CNPq) Laboratório de Poéticas Fronteiriças [http://labfront.tk]. Escritor e editor dos livros&#58; “Relações entre arte, ciência e tecnologia&#58; tendências criativas contemporâneas” (LPF, 2021)&#58; “Jogos digitais&#58; suas realidades lúdicas e múltiplas” (LPF, 2021)&#59; “A memória do digital e outras questões das artes e museologia” (EdUEMG, 2019)&#59; “Percursos contemporâneos&#58; realidades da arte, ciência e tecnologia” (EdUEMG, 2018) dentre outros livros e artigos. Atua&#58; na curadoria, criação e produção no campo da cultura, artes digitais e ciências&#59; como professor em cursos de fronteira como o Curso de Engenharia de Máquinas Biológicas (UFMG, UEMG, UFV e Newton Paiva) e Teatro digital (Rubim Produções)&#59; curadoria de bienal, exposições e residências artísticas. <br><b>Antônio Mozelli</b> é mestre em Artes (UEMG). Possui graduação em Ciência da Computação pela Universidade FUMEC e graduação no Bacharelado em Artes Plásticas na Escola Guignard, UEMG. Atualmente desenvolve pesquisa e trabalhos no campo das artes digitais e explora a utilização de realidade virtual em ambientes de imersão interativa e inteligência artificial. É integrante do grupo de pesquisa (CNPq) Laboratório de Poéticas Fronteiriças [http://labfront.tk].
texto-descricao: “Lave as mãos” é um poema aplicativo, ou poem app, criado em Unity para ser instalado e acessado em SO Android. O poem app busca auxiliar a criação do hábito de se lavar as mãos tendo em vista ser essa uma importante medida de profilaxia após a declaração de pandemia de Sars-Cov-2 pela OMS. Ao mesmo tempo, pensando em promoção da enação (Francisco Varela e Humberto Maturana) criamos condições poéticas para a reflexão do interator. É um aplicado que une a funcionalidade com a fruição poética tendo incluído em sua interface um poema elaborado por Pablo Gobira. Além da discussão sobre a união entre funcionalidade e poética, o aplicativo se relaciona com uma série de trabalhos desenvolvidos no grupo de pesquisa, desenvolvimento e inovação Laboratório de Poéticas Fronteiriças. Os trabalhos poéticos estão relacionados a intervenção na realidade através da relação com acontecimentos históricos ou por meio da criação de tensionamentos nessa realidade. Esta obra foi premiada no SHIFT&#58; projetar o mundo após o fim do mundo, edital de 2020 organizado pelo Consórcio del Sur (Universidade Federal de Goiás, Universidade Federal do Sul e Sudeste do Pará, Universidade de Brasília, Universidade Anhembi-Morumbi, Pontifícia Universidade Católica de Campinas, Universidad de Buenos Aires, Universidad de Chile e Universidad Jorge Tadeo Lozano).
ano: 2020-2021 
---

<img src="/assets/externos/lave-as-maos/lave-as-maos.png" class="img-fluid" alt="PoemAPP"/>
<br>

<p>“Lave as mãos” é um poema aplicativo, ou poem app.</p>

<a href="https://drive.google.com/file/d/1GQs-VyaqaREDc0mulrxqnmrRBgFOQFds/view?usp=sharing"> LINK PARA APK </a> <br>
`Compatível apenas com Android` 

<br>
O "Lave as mãos" foi desenvolvido por Pablo Gobira e Antônio Mozelli, membros do [Laboratório de Poéticas Fronteiriças](http://labfront.tk).
