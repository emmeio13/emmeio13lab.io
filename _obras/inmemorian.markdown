---
layout: obra
title: InMemoriam
thumbnail: /assets/thumbnail/t-inmemorian.png
artista: Grupo Realidades (ECA/USP)
duo-bio: Grupo Realidades (ECA/USP) - Bruna Mayer, Clayton Policarpo, Dario Vargas, Felipe Mamone, Lívia Gabbai, Letícia Brasil, Loren Bergantini, Luca Ribeiro, Miguel Alonso, Paula Perissinotto, Sergio Venancio, Silvia Laurentiz.<br>O Grupo de Pesquisa Realidades (ECA/USP), desde sua fundação em 2010, questiona como lidar coerentemente com sistemas de diferentes formatos de representação imagética. Explora as realidades aumentadas, virtuais e mistas, simulações e emulações, visualizações de dados e inteligência artificial, em amplo espectro contemplando games, sites, arte interativa e instalações que desafiam e redirecionam o termo realidade. O grupo Realidades busca examinar e experimentar as relações entre conhecimentos científicos e conceitos poéticos e filosóficos, em diferentes dimensões sígnicas, sempre provocados pela relação entre arte e tecnologia e a partir de uma experiência estética. É um grupo de pesquisa credenciado e sediado na Escola de Comunicações e Artes da Universidade de São Paulo, licenciado pelo CNPq, e liderado pela Profª. Drª. Silvia Laurentiz.<br> <a href="http://www2.eca.usp.br/realidades"></a>www2.eca.usp.br/realidades
texto-descricao: InMemoriam é uma homenagem às vítimas de covid-19 no Brasil. Os pixels que se desprendem da primeira página da seção “Dos Direitos e Garantias Fundamentais” presente na Constituição Federal de 1988 representam a quantidade de mortes, dia a dia, ao longo do período. O sequenciamento exponencial da pandemia associa o fracasso das políticas públicas e sanitárias nacionais à alienação dos direitos adquiridos.
ano: 2020-2021 
---

<iframe class="frame" src="/assets/externos/in_memorian/index.html"></iframe>
<br>
<a href="/assets/externos/in_memorian/index.html" target="_blank">TELA INTEIRA</a>