---
layout: obra
title: 1MULHERporM2 em Pontos De Fuga
thumbnail: /assets/thumbnail/t-1MULHERporM2.png
artista: 1MULHERporM2
duo-bio: O coletivo 1mulherporM2 é composto por um grupo de 16 artistas. Adriana Bertini, Ana Roberta Lima, Bia Parreiras, Cami Onuki, Carla Venusa, Fernanda Klee, Fulvia Molina, Iara Venanzi, Karen Caetano, Laura Corrêa, Lucrécia Couso, Lynn Carone, Marcia Gadioli, Melissa Haidar, Paula Marina, Tina Leme Scott.
texto-descricao: Idealizado em março de 2019 por Lucrécia Couso, artista e curadora, 1MULHERporM2 é um grupo de mulheres artistas visuais que propõe discussões sobre arte contemporânea, onde cada uma, fazendo uso de seu próprio meio de expressão e deve ocupar um metro quadrado com sua obra, fazendo assim, uma relação literal com esse espaço físico e o ainda limitado espaço da mulher na sociedade. Essas artistas vêm, com suas poéticas, realizando obras e intervenções em espaços públicos e institucionais. Depois de um ano de exposições itinerantes, paralisada por conta da pandemia da Covid-19, oColetivo manteve encontros virtuais, para produzir e propor novas formas de ocupações. Quando pensamos em pontos de fuga, trazemos para a chamada “nova realidade” uma expressão vinda do desenho renascentista, onde através de um ponto no espaço, criamos a ilusão de profundidade numa obra de arte. Através desta perspectiva visual, estamos fazendo um paralelo com a ideia da busca ou descoberta de um novo horizonte no tempo real. A exposição em versão virtual, é um convite para circular em um labirinto formado pelos trabalhos das artistas do coletivo e “entrar” em cada imagem para descobrir o ambiente imaginado e construído para aplicar cada lambe-lambe de acordo com o desejo de cada uma. Esses trabalhos também foram apresentados presencialmente na estação Clínicas do metrô em São Paulo e no Festiva de Fotografia de Paranapiacaba.
ano: 2019-2020 
---

<div class="d-none d-sm-block"> 

<iframe class="frame" style="height:470px;" src="https://i.simmer.io/@mvrgana/pontos-de-fuga-1mulherpm2"></iframe>
<br><br>

<a href="https://i.simmer.io/@mvrgana/pontos-de-fuga-1mulherpm2" target="_blank">TELA INTEIRA</a></div>

<div class="d-block d-sm-none"> 

`A visualização completa esta disponível somente para computador` 

<iframe src="https://www.youtube.com/embed/axkYDukmwu4" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
</div>
<br><br>

<b>1MULHERporM2</b>(Adriana Bertini, Ana Roberta Lima, Bia Parreiras, Cami Onuki, Carla Venusa, Fernanda Klee, Fulvia Molina, Iara Venanzi, Karen Caetano, Laura Corrêa, Lucrécia Couso, Lynn Carone, Marcia Gadioli, Melissa Haidar, Paula Marina, Tina Leme Scott.)