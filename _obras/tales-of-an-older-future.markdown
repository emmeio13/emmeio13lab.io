---
layout: obra
title: Tales Of An Older Future
thumbnail: /assets/thumbnail/t-talesofanolderfuture.png
artista: Alexandre Rangel
bio: https://www.alexandrerangel.art.br/bio.html
texto-descricao: Generative audiovisual composition.
ano: 2020-2021 
---

<iframe class="frame" src="https://www.alexandrerangel.art.br/hydra/older-future/"></iframe>
<br>
<a href="https://www.alexandrerangel.art.br/hydra/older-future/" target="_blank">TELA INTEIRA</a>