---
layout: obra
title: humanomania
thumbnail: /assets/thumbnail/t-humanomania.png
artista: Raísa Curty
bio: https://www.instagram.com/raisa.curty/
texto-descricao: Série de aquarelas feitas a partir de fotografias tiradas com o celular durante a pandemia.
ano: 2021
---
<img src="/assets/externos/humanomania/01.jpg" loop=infinite alt="humanomania" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/externos/humanomania/02.jpg" loop=infinite alt="humanomania" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/externos/humanomania/03.jpg" loop=infinite alt="humanomania" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/externos/humanomania/04.jpg" loop=infinite alt="humanomania" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/externos/humanomania/05.jpg" loop=infinite alt="humanomania" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/externos/humanomania/06.jpg" loop=infinite alt="humanomania" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/externos/humanomania/07.jpg" loop=infinite alt="humanomania" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/externos/humanomania/08.jpg" loop=infinite alt="humanomania" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/externos/humanomania/09.jpg" loop=infinite alt="humanomania" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/externos/humanomania/10.jpg" loop=infinite alt="humanomania" class="img-fluid mx-auto d-block">

