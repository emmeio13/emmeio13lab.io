---
layout: obra
title: c0n.t4ng3r3 4nt3.c3d3r3 (/contangere antecedere/)
thumbnail: /assets/thumbnail/t-c0n.png
artista: Léo Pimentel – mEt[A]²rtᴉstA tr[A]nsmídᴉA da ᴉnc[E]rtƎzA
bio: https://amantedaheresia.blogspot.com
texto-descricao: Experimento poético-filosófico audiovisual de colagem, utilizando apenas softwares livres, inspirado no aforismo 341, “o peso mais pesado” de Nietzsche em seu livro “A Gaia Ciência”. em minha experimentação trato da autoinfecção imaginativa da pessoa artista e seu duplo que se condenam mutuamente a um eterno retorno criativo&#58; “infecte a ti mesmo! imagina-te!”
ano: 2020-2021 
---

<iframe src="https://www.youtube.com/embed/a4r7Bu5QSmE" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>
