---
layout: obra
title: teleportation.com.ar
thumbnail: /assets/thumbnail/t-teleportation.png
artista: Teleportation Staff
duo-bio: Idea y Diseño&#58; Martín Groisman <br>Diseño interactivo, arte digital&#58; Fabricio Costa, Dante Saez, Aimé Guzmán <br>Diseño Sonoro&#58; Alejandro Kauderer <br>Post producción&#58; Javier Pistani <br>Diseño Gráfico&#58; Mica Schiaffino <br>Actúan&#58; Lucila Sol, Bia Borges, Gustavo Luppi
texto-descricao: La irrupción del fenómeno COVID 19 y sus efectos devastadores en todo el planeta ha provocado la aceleración de los tiempos de investigación y desarrollo del proyecto AIT, un laboratorio científico integrado por investigadores de diversos países que trabaja en procura de una solución global ante la necesidad sanitaria de aislamiento social, cierre de fronteras y paralización de actividades presenciales. <br>Teletransportación de seres humanos <br>Debido a que los humanos se componen de muchos miles de átomos, que son fenómenos de frecuencia electromagnética -es decir, no cosas- es teórica y prácticamente posible que estas complejas frecuencias puedan ser minuciosamente decodificadas y transmitidas por medio de haces al vacío celestial para ser recibidas en algún momento y en algún lugar del universo. <br>Al atravesar el umbral cuántico del dispositivo, los átomos se transforman en bits, los cuerpos se “desmaterializan” y la gente está en condiciones de ser lanzada a explorar múltiples universos paralelos. <br>La idea de la teletransportación - una vieja fantasía tecno-científica- se basa en la formulación de la Teoría de Cuerdas, donde el espacio-tiempo  no sería el espacio-tiempo ordinario de cuatro dimensiones, sino un espacio en el que a las cuatro dimensiones convencionales se añaden seis dimensiones compactadas. Por lo tanto, en la Teoría de Cuerdas existe una dimensión temporal, tres dimensiones espaciales ordinarias y seis dimensiones compactadas e inobservables en la práctica.
ano: 2020-2021 
---

<iframe class="frame"  allow="camera;geolocation;fullscreen;" src="https://www.teleportation.com.ar/en/viajar"></iframe>
<br>
<a href="https://www.teleportation.com.ar/en/viajar" target="_blank">TELA INTEIRA</a>