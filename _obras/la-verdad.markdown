---
layout: obra
title: La Verdad
artista: Cecilia Vilca
bio: https://www.ceciliavilca.com 
thumbnail: /assets/thumbnail/t-la-verdad.png
texto-descricao: <b>[ES]</b> Ante la imposibilidad que Keiko Fujimori, heredera del Fujimorismo diga la verdad, creo una inteligencia artificial (IA) de voz que lo haga, concluyo es la única manera. Esto sucede en medio de las elecciones a las que por tercera vez postula la eterna candidata. Aprendo lo que implica entrenar una IA, escucho todos sus discursos-mentiras, desde el 2011 al cierre de campaña 2021. Una tortura archivística que hago de manera hipnótica con la esperanza que este embrión máquina aprenda a decir la verdad. Pero ¿Dónde está la verdad, existe la verdad? Parte de ella fue recogida por la Comisión de la Verdad y la Reconciliación (CVR). Ahí junto a muchos otros, colgado en YouTube, encuentro el testimonio de Liz Rojas Valdez. Tengo la esperanza que de alguna forma esta confrontación audiovisual entre tecnologías, que el proyecto propone, tenga algo de restitución. El dolor no prescribe, la verdad existe, aunque sea artificial y este en pleno aprendizaje. ¿Se puede enseñar a ser decente/corrupto? toda esta historia familiar, esta dinastía de corrupción. Pienso en el paralelo con mi propia vida, siempre votando en contra, siempre sintiendo que no vale mi voz. Liz, yo te prometo, le hare decir a esa voz artificial, que parece un robot del apocalipsis o un balbuceo de un niño, mi (nuestra/la) verdad. K. F. si me quitaste mi voz, tomare la tuya, la tomaremos todos. <br><br> <b>[PT]</b> Dada a impossibilidade de Keiko Fujimori, herdeira do fujimorismo, para falar a verdade, crio uma inteligência artificial de voz para isso, concluo que é o único caminho. Isso acontece em meio às eleições às quais a eterna candidata concorre pela terceira vez. Aprendo o que significa treinar uma Inteligência Artificial, ouço todos os seus discursos-mentiras, de 2011 ao final da campanha de 2021. Uma tortura de arquivo que faço hipnoticamente na esperança de que aquele embrião de máquina aprenda a falar a verdade. Mas onde está a verdade, existe a verdade? Parte dela foi arrecadada pela Comissão de Verdade e Reconciliação (CVR). Lá junto com muitos outros, postados no YouTube, encontro o depoimento de Liz Rojas Valdez. Espero que de alguma forma esse confronto audiovisual entre tecnologias, que o projeto propõe, tenha alguma restituição. A dor não prescreve, a verdade existe, mesmo que seja artificial e em pleno aprendizado. Você pode aprender a ser decente / corrupto? Toda essa história familiar, essa dinastia da corrupção, penso no paralelo com a minha própria vida, sempre votando contra, sempre sentindo que minha voz não vale a pena. Liz, eu prometo a você, farei com que aquela voz artificial, que parece um robô do apocalipse ou um balbucio de criança, diga a minha (nossa/a) verdade. K. F., se você tirou minha voz de mim, eu tirarei a sua, todos nós a tiraremos de você.
ano: 2021 
---
<style type="text/css">
	#concepto {
	padding-top: 80px;
	text-align: left;
	margin-left:auto;
	margin-right:auto;
	width:1200px;
	
}



#ia {
	
	float: left;
	width: 300px;
	clear: both;
    height: 100%;
	padding-top: 250px;
	padding-right: 50px;
	/*background-color:aquamarine;*/
}

#texto {
	width: 300px;
	padding-left: 20px;
	line-height: 175%; 
}

#creditos {
	font-family: 'Space Mono', monospace;
	font-size:12px;
	width: 300px;
	padding-left: 20px;
</style>

<div id="concepto" >
     <div id="ia" >
        <audio controls>
        <source src="/assets/externos/la-verdad/mix90_110s.mp3" type="audio/mp3">
            Tu navegador no soporta audio HTML5.
        </audio>
      </div>
    <div id="video">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/7bw6OcfU5wg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div id= "texto">
      <p style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13.5px;">
        "Yo, Keiko Fujimori, llamé error a tu madre, repetidas veces, Liz Rojas Valdez, negué su desaparición y con ella el dolor que desde tus doce años persiste hasta hoy.<br />
        <br />
        Tu todo Liz, tu alegría se la llevaron quienes defiendo y a quienes glorifico. Pero ese 17 de mayo del año 1991 si existió. Ese horror si sucedió, esa verdad que buscabas,
        siendo tan valiente, si existe, aquí está. <br />
        <br />
        Soy la heredera del dictador genocida Alberto Fujimori, mi padre. Nunca he trabajado en mi vida. Lidero una agrupación mafiosa llamada Fujimorismo que tiene cepas nuevas estas elecciones."<br /> 
    <div id="creditos"><b>IA de voz de Keiko Fujimori en entrenamiento para decir la verdad (sin completar).</b>Período 11 de abril-9 de junio 2021, 352 audios y transcripciones depurados, infinitos videos visualizados,  
       110/90 minutos de entrenamiento de nivel inferior, 83/68 minutos de nivel superior. 31 años, 3 elecciones, varias generaciones de peruanos.<br /><br />
   
  </div> 