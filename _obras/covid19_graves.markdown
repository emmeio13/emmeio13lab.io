---
layout: obra
title: Covid19_Graves
thumbnail: /assets/thumbnail/t-covid19_graves.png
artista:  Suzete Venturelli
duo-bio: Professora titular, artista e pesquisadora da Universidade Anhembi Morumbi e Universidade de Brasília. Bolsista do CNPq.
texto-descricao: O tema contaminação perpassa a proposta Covi19_Graves e sugere que a cada  oportunidade de afetar corresponde a uma capacidade de ser afetado e sentir. Estimulada para apresentar a contaminação por meio dos dados por uma força poética que é o sinal de uma inquietude, a obra converte dados conceitualmente e emocionalmente, a partir de um olhar interno. Os dados, obtidos todos os dias, são convertidos em curvas de Bézier, que é uma curva paramétrica, como linha ou "caminho" usado para criar gráficos vetoriais. Ela consiste em dois ou mais pontos de controle, que definem o tamanho e a forma da linha. O primeiro e o último pontos marcam o início e o fim do caminho, enquanto os pontos intermediários definem a curvatura do caminho. As curvas de Bézier são usadas, nesta obra, para criar linhas curvas em espiral e podem ser redimensionadas para apresentar um caminho que se desenrola num plano regular e se afasta gradualmente. As cores têm como referência uma fotografia anônima de um enterro de vítimas fatais da doença que ocorreu em Brasília, no ano de 2021. O trabalho foi realizado com a participação de Francisco de Paula Barretto.
ano: 2021 
---
 
<iframe class="frame" scrolling="no" src="/assets/externos/covid19_graves/"></iframe>
<br>
<a href="/assets/externos/covid19_graves/" target="_blank">TELA INTEIRA</a>