---
layout: obra
title:  Elegie Pour Lars
thumbnail: /assets/thumbnail/t-elegie-pour-lars.png
artista: Antenor Ferreira
bio: http://antenor.mus.br/?page_id=61  
texto-descricao: Élégie pour Lars é uma criação áudio-visual baseada em pinturas de Lars Tyrenius. A obra é uma homenagem ao artista sueco, um dos pioneiros do abstracionismo na Suécia, falecido neste ano em meio à pandemia de Corona vírus. A obra mescla a paixão que ele cultivava pela arte e pela natureza. A música original foi composta inspirada em telas de Lars Tyrenius. As citações do poeta francês Paul Verlaine referenciam a predileção que Lars Tyrenius nutria pela poesia francesa (que ele fazia questão de traduzir em suas classes) e pela música impressionista.
ano: 2021 
---

<iframe src="https://www.youtube.com/embed/G4Ol1i8I00o" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>