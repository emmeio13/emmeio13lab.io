//dataset: dataset covid19-Brasil...
//Data de captura: 2021-04-06
//Apoio Francisco de Paula Barretto -  basicamente ele fez com que o algoritmo lesse o numero de mortes diarias
//começando do dia atual
//e depois voltando para os dias anteriores 

var t = 1;

var data;
var sourceImage;
var curviness = 0.4;
var sc = 5;
var sc_off = 10;
let history;
var date;
var size = 20;
var url = 'https://teste.umacidadepordia.com.br/';
var index = 0;


function preload() {
  data = loadTable("obito_cartorio.csv", "csv", "header");
  //url https://teste.umacidadepordia.com.br/query?date=2021-04-27&qtd=20
  let today = new Date().toISOString().slice(0, 10)
  date = today;

  //sourceImage = loadImage("tumulo.jpeg");
  sourceImage = loadImage("05-4.jpeg");

  updateData(date, size);
}

function updateData(date) {
  loadData(url, date, size).then((data) => {
    history = data;
    index = 0;
    t = 0;
    console.log(history);
  });
}

function loadData(url, date, size) {
  return fetch(url + 'query?date=' + date + '&qtd=' + size)
    .then(
      function(response) {
        if (response.status !== 200) {
          console.log('Error. Status Code: ' +
            response.status);
          return;
        }
        return response.json().then(function(data) {
          return data;
        });
      }
    )
    .catch(function(err) {
      console.log('Fetch Error :-S', err);
    });
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  sourceImage.loadPixels();
  noFill();
  background(0);
  frameRate(1000);
  strokeWeight(5);
  
}

function draw() {
  // vai para o centro (0, 0)
  translate(width / 2, height / 2);
  // rotaciona
  rotate(t / 10);

  // calcula os varios pontos
  var tr = t;

  var x1 = lerpData(tr, 30, sc) + sc_off;
  var y1 = lerpData(tr, 5, sc) + sc_off;

  var x2 = lerpData(tr, 6, sc) + sc_off;
  var y2 = lerpData(tr, 7, sc) + sc_off;

  var x3 = lerpData(tr, 8, sc) + sc_off;
  var y3 = lerpData(tr, 10, sc) + sc_off;

  var x4 = lerpData(tr, 12, sc) + sc_off;
  var y4 = lerpData(tr, 13, sc) + sc_off;

  var x5 = lerpData(tr, 15, sc) + sc_off;
  var y5 = lerpData(tr, 17, sc) + sc_off;

  // Cores da imagem  
  var pixel_colour = getPixelColour();
  stroke(pixel_colour);
  //stroke(30);

  // desnha curvas Bezier - tentativa e erro
  var offset = x2 - x1 + y2 - y1;
  offset *= curviness;
  bezier(x1, y1, x1 + offset, y1 - offset, x2 - offset, y2 + offset, x2, y2);

  offset = x3 - x2 + y3 * y2;
  offset *= curviness;
  bezier(x2, y2, x2 * offset, y2 - offset, x3 - offset, y3 + offset, x3, y3);

  offset = x4 - x3 + y4 - y3;
  offset *= curviness;
  bezier(x3, y3, x3 + offset, y3 + offset, x4 + offset, y4 * offset, x4, y4);

  offset = x1 - x4 + y1 - y4;
  offset *= curviness;
  bezier(x4, y4, x4 + offset, y4 - offset, x1 + offset, y1 - offset, x1, y1);

  offset = x1 + x5 + y1 * y4;
  offset *= curviness;
  bezier(x5, y5, x5 + offset, y5 - offset, x1 + offset, y1 - offset, x1, y1);

  // fazer isso a cada frame por algum motivo
  translate(width / -2, height / -2);

  // Advance the time a bit
  t = t + 0.1;
  if (history) {
    if (t >= history[index].daily) {
      getNextData();
    }
  }
}

function getNextData() {
  if (index < size) {
    index++;
    t = 0;
    date = history[index].date;
    //console.log(index + "date" + date + " " + history[index].daily);
  } else {
    t = 0;
    updateData(date, size);
    //console.log("request new date");
  }
}

function getPixelColour() {
  i = parseInt(t) % (sourceImage.width * sourceImage.height);
  c = color(sourceImage.get(i % sourceImage.width, floor(i / sourceImage.width)));
  return color(red(c), green(c), blue(c), 50);
}

// função linear de innterpolação
function lerpData(t, c, scale) {
  r = floor(t);
  rNext = r + 1;
  // se está na última linha, olha para a primeira 
  if (rNext >= data.getRowCount()) {
    rNext = 0;
  }
  var val = scale * parseInt(data.getString(r, c));
  var valNext = scale * parseInt(data.getString(rNext, c));
  var delta = map(t - r, 0, 1, 0, valNext - val);
  return val + delta;
}