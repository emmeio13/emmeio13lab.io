var threshold = 50;
var prop = 10;//fator de proporÃ§Ã£o entre nÃºmero de pontos solÃºveis e nÃºmero de mortos da Covid

var acumortes;
var diamortes;

var img;
var table;
var dotsOrig = [];
var dots = [];
var diss = [];

var dot, d, h;
var wx, wy;
var vpan, vpy, vmy;

var pg;

var diss_acu;
var diss_dia;
var dayCount;

var slider;
var sliderW = 300;
var curSliderValue;
var daytxt;
var acctxt;
var info;
var modal;
var modalOn;

var pos;
var mobileMode;

var myCanvas;


function teste ()
{

    //table = loadTable('/assets/externos/in_memorian/dados.csv', 'ssv', 'header');

    table = loadTable('http://arquivo.medialab.unb.br/cors/www2.eca.usp.br/realidades/inmemoriam/dados.csv', 'ssv', 'header');
var linha = table.getRowCount();
console.log(linha);
if (teste > 0){
  console.log("CSV ok");
}
else {
  console.log("CSV nao");
    //table = loadTable('assets/externos/in_memorian/dados.csv', 'ssv', 'header');

}

}


function preload() {
  img = loadImage("pg1p.jpg"); 

  let url = 'https://api.allorigins.win/raw?url=http://www2.eca.usp.br/realidades/inmemoriam/dados.csv';
  table = loadTable(url, 'ssv', 'header');


}


function setup() {
  myCanvas = createCanvas(1200, 600, WEBGL);
  

  // device detection
  if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
      || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
      mobileMode = true;
  }

  if(mobileMode) {
    resizeCanvas(windowWidth,windowHeight);
  } else if(windowWidth < 1200 && windowWidth >= 800) {
    resizeCanvas(800, 400);
  } 
  myCanvas.position((windowWidth - width)/2,(windowHeight - height)/2);
  myCanvas.parent("canv");
  myCanvas.id('obra');
  
  quantDias = table.getRowCount();

  pos = myCanvas.position();
  slider = createSlider(0,quantDias-1,quantDias-1,1);
  slider.position(pos.x,pos.y+35);
  slider.size(sliderW);
  slider.mouseReleased(sliderChange);
  slider.class('slider')
  curSliderValue = slider.value();

  acumortes = table.getString(quantDias-1,5);
  diamortes = table.getString(quantDias-1,6);
  daytxt = createP(converteDia(table.getString(quantDias-1,0)));
  daytxt.style('font-weight','bold');
  daytxt.position(pos.x+15,pos.y+60);
  acctxt = createP(converteMilhares(acumortes) + " (<span class='redtxt'>+" + diamortes + "</span>) mortos");
  acctxt.style('font-weight','bold');
  acctxt.position(pos.x+15,pos.y);


  info = createButton(' ');
  info.class('infobtn');
  info.position(pos.x+15,pos.y+height-30);
  info.mousePressed(toggleModal);

  modal = select('#modal');
  modal.position(pos.x,pos.y)
  modal.size(width,height);
  modal.hide();
  modalOn = false;

  close = createButton(' ');
  close.class('closebtn');
  close.position(15,height-30);
  close.parent("modal")
  close.mousePressed(toggleModal);

  img.loadPixels();
  for(i = 0;i<img.pixels.length;i+=4) {
    r = img.pixels[i]
    g = img.pixels[i+1]
    b = img.pixels[i+2]
    if((r+g+b)/3 <= 255 - threshold) {
      //pixel nÃ£o Ã© branco
      x = (i/4)%img.width - img.width/2;
      y = floor((i/4)/img.width) - img.height/2 + 150;
      dotsOrig.push(new Dot(x,y,r,g,b));
    }
  }
  img.updatePixels();
  
  dotsOrig = shuffle(dotsOrig);

  wy = -800;
  
  noStroke();

  init();
}

function converteDia(s) {
  return(s[8]+s[9]+"-"+s[5]+s[6]+"-"+s[0]+s[1]+s[2]+s[3]);
}

function converteMilhares(n) {
  var s = ""+n;
  var r = "";
  var pcount = 0;
  for(i = s.length-1;i>=0;i--) {
    r = s[i]+r;
    pcount++;
    if(i > 0 && pcount % 3 == 0) r = "."+r;
  }
  return r;
}

function touchEnded() {
  if(slider.value() != curSliderValue) {
    curSliderValue = slider.value();
    sliderChange();
  }
}

function sliderChange(e) {
  acumortes = table.getString(slider.value(),5);
  diamortes = table.getString(slider.value(),6);
  daytxt.html(converteDia(table.getString(slider.value(),0)));
  acctxt.html(converteMilhares(acumortes) + " (<span class='redtxt'>+" + diamortes + "</span>) mortos");
  
  init();
}

function toggleModal() {
  if(modalOn) {
    modal.hide();
    modalOn = false;
  } else {
    modal.show();
    modalOn = true;
  }
}

function windowResized() {
  if(myCanvas) {
    if(mobileMode) {
      resizeCanvas(windowWidth,windowHeight);
    } else {
      myCanvas.position((windowWidth - width)/2,(windowHeight - height)/2);
      if(windowWidth >= 1200) {
        resizeCanvas(1200,600);
      } else if(windowWidth < 1200) {
        resizeCanvas(800, 400);
      } 
    }
    pos = myCanvas.position();
    slider.position(pos.x,pos.y+35);
    daytxt.position(pos.x+15,pos.y+60);
    acctxt.position(pos.x+15,pos.y);
    info.position(pos.x+15,pos.y+height-30);
    modal.position(pos.x,pos.y);
    modal.size(width,height);
    //close.position(15,height-30);
  }
}

function init() {
  diss_acu = acumortes/prop;
  diss_dia = diamortes;
  dayCount = 0;

  dots = [...dotsOrig];//clona o array
  dayCount = 0;
  diss = [];

  let dt;
  //reset de posiÃ§Ãµes z
  for(i = 0;i<dots.length;i++) {
    dots[i].reset();
  }

  pg = createGraphics(img.width,img.height);
  pg.image(img,0,0);
  pg.noStroke();
  pg.fill(255);

  //dissoluÃ§Ã£o inicial do valor acumulado no dia
  for(i = 0;i < diss_acu; i++) {
    dot = dots.pop();
    pg.square(dot.x + img.width/2,dot.y + img.height/2 -150,1)
  }
}

function draw() {
  background(255);

  if(vpan) {
    wy = vpy - (vmy-mouseY);
    wy = min(wy,600);
    wy = max(wy,-900);
  }
  translate(0,wy,0);

  image(pg,-pg.width/2,-pg.height/2 + 150)
  
  if(dayCount < diss_dia) {
    for(i=0;i<floor(random(1,5));i++) {
      try {
        dot = dots.pop();
        diss.push(dot);
        dayCount++;
      } catch(e) {
        console.log("acabaram os pixels");
      }
    }
  }
  
  for(i = 0;i < diss.length; i++) {
    if(diss[i].z == 0) {
      pg.fill(255);
      pg.square(diss[i].x + img.width/2,diss[i].y + img.height/2 -150,1)
    }

    diss[i].move();
    fill(diss[i].r,diss[i].g,diss[i].b,255-(diss[i].z/300)*255);
    push();
    translate(diss[i].x,diss[i].y,diss[i].z);
    box(1);
    pop();
    
    if(diss[i].z > 300) {
      diss.splice(i,1)
    }
  }


  //wx = map(constrain(mouseX,30,width-30),30,width-30, width/2 - 540,width/2 - 60);
  if(mobileMode) {
    camera(150,0, 150, -300, 0, 0, 0, 1, 0);
  } else {
    camera(150,0, 100, 0, 0, 0, 0, 1, 0);
  }
}
  

function mousePressed() {
  if(mouseX > sliderW || mouseY > 70) {
    vpan = true;
    vmy = mouseY;
    vpy = wy;
  }
}

function mouseReleased() {
  vpan = false;
}

class Dot {
  constructor(x,y,r,g,b) {
    this.x = x;
    this.y = y;
    this.z = 0;
    this.r = r;
    this.g = g;
    this.b = b;
  }
  
  move() {
    this.z += 1;
  }

  reset() {
    this.z = 0;
  }
}