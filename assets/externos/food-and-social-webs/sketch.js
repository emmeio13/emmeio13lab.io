/*
Sketch developed in collaboration by _ 
Amanda Laubmeier <Amanda.Laubmeier@ttu.edu>
Fanqi Zeng <fanqi.zeng@bristol.ac.uk>
Luis Mata <lmata@unimelb.edu.au>
Marília Bergamo <marilialb@ufmg.br>
Naveen Srivatsav <naveen.srivatsav@pwc.com>

Social_Food NETs
*/

let numberOfNodes = 10;
let nodes = [];
let luck = [];
let strokeS = [];
let timer = 5;
let P_explode = false;
let newP;

function setup() {
  createCanvas(600, 600, WEBGL);
  for (let x = 0; x < numberOfNodes; x++) {
      nodes[x] = new node(
        random(-400,400),
        random(-400,400),
        random(-400,400),
        random(50,150)
      );
  }
  
  for(let y = 0; y < 180; y++){
    luck[y] = int(random(10));
    strokeS[y] = int(random(0.2,1));
  }
  
  newP = int(random(0,numberOfNodes));
  //frameRate(2);
}

function draw() {
  background(255);
  lights();
  for (let x = 0; x < numberOfNodes; x++) {
    //nodes[x].rotate();
    nodes[x].display();
  }
  //print("noloop");
  // if the frameCount is divisible by 60, then a second has passed. it will stop at 0
  if (frameCount % 60 == 0 && timer > 0) {
    //print(timer);
    timer = timer-1;
  }
  if (timer == 0){
    //print("entrou");
    P_explode = true;
  }
  
  //TEST of connections view
  //Connecting almost everyone to the next one
  stroke(150,50);
  strokeWeight(1);
  for(let i = 0; i < numberOfNodes-1; i++){
    for(let j = 0; j < 180; j++){
      let localX1 = nodes[i].x + nodes[i].microNodes[j].x;
      let localY1 = nodes[i].y + nodes[i].microNodes[j].y;
      let localZ1 = nodes[i].z + nodes[i].microNodes[j].z;
      let localX2 = nodes[i+1].x + nodes[i+1].microNodes[j].x;
      let localY2 = nodes[i+1].y + nodes[i+1].microNodes[j].y;
      let localZ2 = nodes[i+1].z + nodes[i+1].microNodes[j].z;
      if((nodes[i].alive)&&nodes[i+1].alive){
        //luck[j] = int(random(10));
        strokeS[j] = random(0.2,1);
        if(luck[j]< 5){
          strokeWeight(strokeS[j]);
          line(localX1,localY1,localZ1,localX2,localY2,localZ2);
        }
      }
    }
  }

  //drag to move the world.
  orbitControl();
  
}

function mousePressed() {
  if (mouseX > 0 && mouseX < 100 && mouseY > 0 && mouseY < 100) {
    let fs = fullscreen();
    fullscreen(!fs);
    noCursor();
  }
}

/* CLASSES */

class node{
  constructor(xpos, ypos, zpos, r) {
    this.x = xpos;
    this.y = ypos;
    this.z = zpos;
    this.radius = r;
    this.s = 0;
    this.t = 0;
    this.lastx = 0;
    this.lasty = 0;
    this.lastz = 0;
    this.numMicroNodes = 180; // based on the angle, not supposed to be changed
    this.rotateMe = false;
    this.hasT = false;
    this.alive = true;
    
    this.microNodes = []; // array of Jitter objects
    let co;
    co = int(random(1,7));
    //co = 8; 
    while (this.t < this.numMicroNodes) {
      this.microNodes.push(new microNode());
      switch(co){
        case 1:
          this.microNodes[this.t].setColor('R');
          this.hasT = true;
          break;
        case 2:
          this.microNodes[this.t].setColor('G');
          break;
        case 3:
          this.microNodes[this.t].setColor('B');
          break;
        case 4:
          this.microNodes[this.t].setColor('Y');
          break;
        case 5:
          this.microNodes[this.t].setColor('M');
          break;
        case 6:
          this.microNodes[this.t].setColor('C');
          break;
        case 8:
          this.microNodes[this.t].setColor('E');
          break;
      }
      this.t += 1;
    }
  } 
  
  rotate(){
    this.rotateMe = true;
  }
  
  checkConections(){
    //test acessing others nodes
    let thisNode = new node(0,0,0,0);
    thisNode = this;
    for (let n = 0; n < numberOfNodes; n++) {
      if(thisNode != nodes[n]){
          
      }
    }
  }
  
  display(){  
    push();
    this.t = 0;
    translate(this.x, this.y, this.z);
    if (this.rotateMe){
      rotateY(frameCount * 0.003);
      rotateX(frameCount * 0.004);
    }
    while (this.t < this.numMicroNodes) {
      this.s += 18;
      let radianS = radians(this.s);
      let radianT = radians(this.t);
      let thisx = 0 + (this.radius * cos(radianS) * sin(radianT));
      let thisy = 0 + (this.radius * sin(radianS) * sin(radianT));
      let thisz = 0 + (this.radius * cos(radianT));
      if(this.hasT){
        if((P_explode)&&(this.alive)){
            this.microNodes[this.t].display(0, 0, 0,0);
            this.alive = false;
            //choose somewhere else to put pressure
            nodes[0].alive = true;
            nodes[0].hasT = true;
            P_explode = false;
        } else if (this.alive) {
          this.microNodes[this.t].display(thisx, thisy, thisz, this.radius/int(random(2,8)));
        }
      } else if (this.alive){
        this.microNodes[this.t].display(thisx, thisy, thisz, this.radius/int(random(6,8)));
      }
      this.lastx = thisx;
      this.lasty = thisy;
      this.lastz = thisz;
      this.t += 1;
    }   
    /*
      push();
      noStroke();
      translate(0, 0, 100);
      fill(0, random(100,200));
      torus(this.radius*1.2,int(random(50,100)));
      pop();
      */
    if((this.hasT)&&(this.alive)){
      push();
      noStroke();
      translate(0, 0, 0);
      fill(120, random(100,200));
      torus(this.radius*1.2,int(random(50,80)));
      pop();
    }
    pop();
  } 
}

class microNode{
  constructor() {
    this.x = 0;
    this.y = 0;
    this.z = 0;
    this.RColor = int(random(255));
    this.GColor = int(random(255));
    this.BColor = int(random(255));
  }
  
  display(x, y, z, r){  
    this.x = x;
    this.y = y;
    this.z = z;
    push();
    translate(x, y, z);
    noStroke();
    fill(this.RColor,this.GColor,this.BColor);
    sphere(r);  
    pop();    
  } 
  
  setColor(letter){
    switch(letter) {
      case 'R': 
        this.RColor = int(random(50,255));
        this.GColor = int(random(0,50));
        this.BColor = int(random(0,50));
        break;
      case 'G': 
        this.RColor = int(random(0,50));
        this.GColor = int(random(50,255));
        this.BColor = int(random(0,50));
        break;
      case 'B': 
        this.RColor = int(random(0,50));
        this.GColor = int(random(0,50));
        this.BColor = int(random(50,255));
        break;
      case 'Y': 
        this.RColor = int(random(150,255));
        this.GColor = int(random(150,255));
        this.BColor = 0;
        break;
      case 'M': 
        this.RColor = int(random(100,255));
        this.GColor = 0;
        this.BColor = int(random(100,255));
        break;
      case 'C': 
        this.RColor = 0;
        this.GColor = int(random(100,255));
        this.BColor = int(random(100,255));
        break;
      case 'E':
        this.RColor = int(random(60,65));
        this.GColor = int(random(60,65));
        this.BColor = int(random(60,65));
        break;
    }
  }
}